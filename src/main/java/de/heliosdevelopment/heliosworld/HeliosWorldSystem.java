package de.heliosdevelopment.heliosworld;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class HeliosWorldSystem extends JavaPlugin {
    private static HeliosWorldSystem instance;
    private WorldHandler worldHandler;

    @Override
    public void onEnable() {
        instance = this;
        worldHandler = new WorldHandler();
        getCommand("world").setExecutor(new WorldCommand());
    }

    @Override
    public void onDisable() {
        try {
            worldHandler.saveWorlds();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static HeliosWorldSystem getInstance() {
        return instance;
    }

    public WorldHandler getWorldHandler() {
        return worldHandler;
    }
}
