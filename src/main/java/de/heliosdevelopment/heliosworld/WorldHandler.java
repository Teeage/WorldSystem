package de.heliosdevelopment.heliosworld;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class WorldHandler {
    private final List<HeliosWorld> worldList;
    private final File folder = new File(HeliosWorldSystem.getInstance().getDataFolder(), "worlds");

    public WorldHandler() {
        this.worldList = new ArrayList<>();
        if (!folder.exists())
            folder.mkdirs();
        for (File file : folder.listFiles()) {
            YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
            HeliosWorld world = new HeliosWorld(UUID.fromString(cfg.getString("uuid")), cfg.getString("creator"), cfg.getString("name"), cfg.getString("type"), cfg.getString("date"), cfg.getBoolean("load"));
            worldList.add(world);
            if (world.isLoaded())
                Bukkit.createWorld(new WorldCreator(world.getName()));
        }
    }

    public void addWorld(String name, String creator, String type) {
        File file = new File(folder, name + ".yml");
        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        UUID uuid = UUID.randomUUID();
        String date = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date());
        cfg.set("id", uuid.toString());
        cfg.set("creator", creator);
        cfg.set("name", name);
        cfg.set("type", type);
        cfg.set("date", date);
        cfg.set("load", Bukkit.getWorld(name) != null);
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        worldList.add(new HeliosWorld(uuid, creator, name, type, name, Bukkit.getWorld(name) != null));
    }

    public void deleteWorld(HeliosWorld world) {
        String name = world.getName();
        if (world.isLoaded())
            Bukkit.unloadWorld(name, false);
        File file = new File(folder, name + ".yml");
        file.delete();
        worldList.remove(world);
        deleteFolder(new File(name));

    }

    public void saveWorlds() throws IOException {
        for (HeliosWorld world : worldList) {
            File file = new File(folder, world.getName() + ".yml");
            YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
            cfg.set("load", world.isLoaded());
            cfg.save(file);
        }
    }

    public HeliosWorld getWorld(String name) {
        for (HeliosWorld world : worldList) {
            if (world.getName().equalsIgnoreCase(name))
                return world;
        }
        return null;
    }

    private boolean deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory())
                    deleteFolder(file);
                else
                    file.delete();
            }
        }
        return folder.delete();
    }

    public List<HeliosWorld> getWorldList() {
        return worldList;
    }
}
