package de.heliosdevelopment.heliosworld;

import net.minecraft.server.v1_8_R3.WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

public class WorldCommand implements CommandExecutor {

    private final HeliosWorldSystem plugin = HeliosWorldSystem.getInstance();
    private final WorldHandler worldHandler = plugin.getWorldHandler();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (!command.getName().equalsIgnoreCase("world"))
            return true;
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (!player.hasPermission("worldsystem.use"))
                return true;
            if (args.length == 0) {
                player.sendMessage("§e/world list");
                player.sendMessage("§e/world create (name) (type)");
                player.sendMessage("§e/world remove (name)");
                player.sendMessage("§e/world load (name)");
                player.sendMessage("§e/world unload (name)");
                player.sendMessage("§e/world import (name)");
                player.sendMessage("§e/world info (name)");
                player.sendMessage("§e/world tp (name)");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("list")) {
                    player.sendMessage("§eWelten");
                    player.sendMessage("");
                    for (HeliosWorld world : worldHandler.getWorldList()) {
                        if (world.isLoaded())
                            player.sendMessage("§a" + world.getName());
                        else
                            player.sendMessage("§c" + world.getName());
                    }

                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("remove")) {
                    HeliosWorld world = worldHandler.getWorld(args[1]);
                    if (world == null) {
                        player.sendMessage("§cDie Welt konnte nicht gefunden werden");
                        return true;
                    }
                    worldHandler.deleteWorld(world);
                    player.sendMessage("§aDie Welt " + world.getName() + " wurde erfolgreich gelöscht");
                } else if (args[0].equalsIgnoreCase("tp")) {
                    HeliosWorld world = worldHandler.getWorld(args[1]);
                    if (world == null) {
                        player.sendMessage("§cDie Welt konnte nicht gefunden werden");
                        return true;
                    }
                    if (world.isLoaded())
                        player.teleport(Bukkit.getWorld(world.getName()).getSpawnLocation());
                    else
                        player.sendMessage("§cDie Welt muss zuerst geladen werden.");
                } else if (args[0].equalsIgnoreCase("load")) {
                    HeliosWorld world = worldHandler.getWorld(args[1]);
                    if (world == null) {
                        player.sendMessage("§cDie Welt konnte nicht gefunden werden");
                        return true;
                    }
                    world.setLoad(true);
                    player.sendMessage("§aDu hast die Welt erfolgreich geladen.");
                } else if (args[0].equalsIgnoreCase("unload")) {
                    HeliosWorld world = worldHandler.getWorld(args[1]);
                    if (world == null) {
                        player.sendMessage("§cDie Welt konnte nicht gefunden werden");
                        return true;
                    }
                    world.setLoad(false);
                    player.sendMessage("§aDie Welt ist nun nicht mehr geladen.");
                } else if (args[0].equalsIgnoreCase("import")) {
                    worldHandler.addWorld(args[1], player.getName(), "normal");
                    player.sendMessage("§aDu hast die Welt importiert.");
                } else if (args[0].equalsIgnoreCase("info")) {
                    HeliosWorld world = worldHandler.getWorld(args[1]);
                    if (world == null) {
                        player.sendMessage("§cDie Welt konnte nicht gefunden werden");
                        return true;
                    }
                    player.sendMessage("§aInformationen zu §7" + world.getName());
                    player.sendMessage("§eUUID: §7" + world.getId());
                    player.sendMessage("§eName: §7" + world.getName());
                    player.sendMessage("§eCreator: §7" + world.getCreator());
                    player.sendMessage("§eType: §7" + world.getType());
                    player.sendMessage("§eDate: §7" + world.getCreationDate());
                    player.sendMessage("§eLoaded: §7" + world.isLoaded());

                }

            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("create")) {
                    switch (args[2].toLowerCase()) {
                        case "normal":
                        case "flat":
                        case "large_biomes":
                        case "amplified":
                            WorldCreator.name(args[1]).environment(World.Environment.NORMAL).type(WorldType.valueOf(args[2].toUpperCase())).createWorld();
                            worldHandler.addWorld(args[1], player.getName(), args[2].toUpperCase());
                            player.sendMessage("§aDu hast die Welt §7" + args[1] + " §avom Typ §7" + args[2].toUpperCase() + " §eerstellt.");
                            break;
                        case "end":
                        case "nether":
                            WorldCreator.name(args[1]).environment(World.Environment.valueOf(args[2].toUpperCase())).createWorld();
                            worldHandler.addWorld(args[1], player.getName(), args[2].toUpperCase());
                            player.sendMessage("§aDu hast die Welt §7" + args[1] + " §avom Typ §7" + args[2].toUpperCase() + " §eerstellt.");
                            break;
                        case "empty":
                            worldHandler.addWorld(args[1], player.getName(), args[2].toUpperCase());
                            WorldCreator worldCreator = new WorldCreator(args[1]);
                            ChunkGenerator chunkGenerator = new ChunkGenerator() {
                                @Override
                                public byte[] generate(World world, Random random, int x, int z) {
                                    return new byte[world.getMaxHeight() / 16];
                                }
                            };
                            worldCreator.generator(chunkGenerator);
                            Bukkit.getWorlds().add(worldCreator.createWorld());
                            player.sendMessage("§aDu hast die Welt §7" + args[1] + " §avom Typ §7" + args[2].toUpperCase() + " §eerstellt.");
                            break;
                        default:
                            player.sendMessage("§cKonnte den Welttypen nicht finden.");
                            break;

                    }
                }
            }
        }
        return true;
    }
}
